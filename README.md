# HavApp - Weather App
Test App for Web Developer position in MetaQuotes

-----

### Technical Specification
https://blog.rensite.ru/article/tz-07-05-2018

-----

### Technology Stack
JavaScript, HTML5(Canvas, IDB), CSS3, WebPack

-----

### How to use
Navigate your browser to [https://havapp.rensite.ru](https://havapp.rensite.ru) and follow your intuition.

-----

### What is optimized

1. Dataset is grouped by month inside of IDB, year index added.
2. 4 layered canvases in order to reduce app mode switch time.
3. Canvas updates only when changing __dataset range__ or window resize (to fit).
4. Grid canvas updates only on window resize (to fit).
5. Charts are simplified using Ramer–Douglas–Peucker algorithm. Up to 35 times less points to draw (1881 - 2006).
6. All calculations are inside of the webworker.
7. UI 😉

-----

### How it works
App launch ```init()```

1. First app checks url hash in order to determine app mode (temperature/precipitation) ```checkhash()```
2. Opening IDB connection
3. Reading ```readNext()``` weather data according app mode ```var page = temperature/precipitation```
4. Showing loader animation ```loader(true)```. Navigation and buttons are still accessible.
5. __See if IDB dataset is empty__
6. Reducing total number of points need to be drawn. Ramer–Douglas–Peucker algorithm. Inside of worker ``myWorker```
7. Drawing chart ```myDraw()```
8. Hiding loader animation ```loader(false)```.

Drawing grids ```axisesDraw()```

##### if IDB dataset is empty
```show[page].length === 0```

1. Fetching dataset from server ```loadDoc()```
2. Putting monthly values into IDB ```putNext()```
3. Back to __How it works - 3__

##### OnResize
App relaunch ```init()```
Drawing grids ```axisesDraw()```

##### On years change
App relaunch ```init()```
Grids remains the same.

-----

### Development
Clone this repo to your local machine, change the dir and run webpack command:

```npm run develop```

The project will be running at:
[http://localhost:8080](http://localhost:8080)

-----

### Deployment

Run following:

```npm run production```

-----

### Did you know?
Hava - stands for "һава" (Татарча) - 'air'

-----

### Contacts
[telegram](https://t.me/renman)

[email](mailto://rensite@yandex.ru)
