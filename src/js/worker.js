// Ramer–Douglas–Peucker algorithm JS implementation
// https://en.wikipedia.org/wiki/Ramer–Douglas–Peucker_algorithm

self.onmessage = function (data) {
  var points = data.data[0];
  var tolerance = data.data[1];
  simplifyPath(points, tolerance);
}

function simplifyPath (points, tolerance) {
	// helper classes
	var Vector = function( x, y ) {
		this.x = x;
		this.y = y;

	};

	var Line = function( p1, p2 ) {
		this.p1 = p1;
		this.p2 = p2;

		this.distanceToPoint = function( point ) {
			// Slope
			var m = ( this.p2.y - this.p1.y ) / ( this.p2.x - this.p1.x ),
				// Y offset
				b = this.p1.y - ( m * this.p1.x ),
				d = [];
			// Distance to the linear equation
			d.push( Math.abs( point.y - ( m * point.x ) - b ) / Math.sqrt( Math.pow( m, 2 ) + 1 ) );
			// Distance to p1
			d.push( Math.sqrt( Math.pow( ( point.x - this.p1.x ), 2 ) + Math.pow( ( point.y - this.p1.y ), 2 ) ) );
			// Distance to p2
			d.push( Math.sqrt( Math.pow( ( point.x - this.p2.x ), 2 ) + Math.pow( ( point.y - this.p2.y ), 2 ) ) );

			// Return the smallest distance
			return d.sort( function( a, b ) {
				return ( a - b ); //Causes an array to be sorted numerically and ascending
			} )[0];
		};
	};

	function douglasPeucker (points, tolerance) {
		if (points.length <= 2) {
			return [points[0]];
		}

		var returnPoints = [],
			// Make line from start to end
      p1 = {
          x: 0,
          y: points[0]
        },
      p2 = {
          x: points.length - 1,
          y: points[points.length - 1]
        },
			line = new Line(p1, p2),
			// Find the largest distance from intermediate poitns to this line
			maxDistance = 0,
			maxDistanceIndex = 0,
			p;

    for (var i = 1; i <= points.length - 2; i++) {
      var point = {
          x: i,
          y: points[ i ]
        };

      var distance = line.distanceToPoint( point );

      if (distance > maxDistance) {
				maxDistance = distance;
				maxDistanceIndex = i;
			}
		}

		// check if the max distance is greater than our tollerance allows
		if ( maxDistance >= tolerance ) {
			p = points[maxDistanceIndex];
			line.distanceToPoint( p, true );
			// include this point in the output
			returnPoints = returnPoints.concat( douglasPeucker( points.slice( 0, maxDistanceIndex + 1 ), tolerance ) );
			// returnPoints.push( points[maxDistanceIndex] );
			returnPoints = returnPoints.concat( douglasPeucker( points.slice( maxDistanceIndex, points.length ), tolerance ) );
		} else {
			// ditching this point
			p = points[maxDistanceIndex];
			line.distanceToPoint( p, true );
			returnPoints = [points[0]];
		}
		return returnPoints;
	};

	var arr = douglasPeucker(points, tolerance);
	// always have to push the very last point on so it doesn't get left off
	arr.push(points[points.length - 1 ]);
	// eturn arr;
  self.postMessage(arr);
};
