/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(2);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

/**
 * HavApp - Weather App
 * Test Task for MetaQuotes
 * @author  renman
 */

// Plot canvas
var canvasOne = document.getElementById("plotOne"); // Our canvas
var cotxt = canvasOne.getContext("2d"); // Let's get drawing context

// Plot canvas
var canvasTwo = document.getElementById("plotTwo"); // Our canvas
var cttxt = canvasTwo.getContext("2d"); // Let's get drawing context

// Grid canvas
var gridOne = document.getElementById("gridOne"); // Our canvas
var gotxt = gridOne.getContext("2d"); // Let's get drawing context

// Grid canvas
var gridTwo = document.getElementById("gridTwo"); // Our canvas
var gttxt = gridTwo.getContext("2d"); // Let's get drawing context

// vars
// var show[page] = []; // Values to visualize
var show = [{ temperature: [], precipitation: [] }]; // Values to visualize
var yrs = [1881, 2006]; // Initial years range. Here we did a small hack and set it manually according dataset
var yrsLimit = [1881, 2006]; // Default years range. The same hack as above
var padding = 20; // Plot paddings
var db;

// App modes settings
var page = 'temperature '; //  mode temperature/precipitation
var modes = [{
  temperature: {
    url: 'data/temperature.json',
    y: 'C°',
    db: 'temperature'
  },
  precipitation: {
    url: 'data/precipitation.json',
    y: 'mm',
    db: 'precipitation'
  }
}];

// IDB settings
var IDBSetting = {
  name: "havaDB",
  version: 2,
  tables: [{
    temperature: {
      tableName: "temperature",
      keyPath: "yid",
      autoIncrement: true,
      index: ["yid", "year"],
      unique: [true, false]
    },
    precipitation: {
      tableName: "precipitation",
      keyPath: "yid",
      autoIncrement: true,
      index: ["yid", "year"],
      unique: [true, false]
    }
  }]
};

// Checking IDB prefixes
window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

// Checking browser's IDB support
if (!window.indexedDB) {
  window.alert("Your browser doesn't have proper IndexedDB support. We are sorry.");
}

// Checking browser's webworker support
if (typeof Worker === "undefined") {
  window.alert("Your browser doesn't have proper Worker support. We are sorry.");
}

/////////////////////
///// Prototypes /////
/////////////////////

// Checks if number is in between a & b
Number.prototype.between = function (a, b) {
  var min = Math.min(a, b),
      max = Math.max(a, b);

  return min <= this && this <= max;
};

// Checks if start year erlier than finish year
Number.prototype.follows = function (n, k) {
  var nk = k === 0 ? 1 : -1;
  var diff = (n - this) * nk;

  if (diff > 0) {
    return true;
  } else {
    return false;
  }
};

/////////////////////
///// Functions /////
/////////////////////


///// Main function
function init() {
  // Highlight current mode
  checkhash();

  // Let's open IDB
  var request = indexedDB.open(IDBSetting.name, IDBSetting.version);

  // Sir, we've encountered IDB connection error, see the log
  request.onerror = function (event) {
    console.log('Error', event);
  };

  // Everything is OK, let's start our App
  request.onsuccess = function (idb) {
    db = event.target.result;
    readNext();
  };

  // We need to upgrade our DB
  request.onupgradeneeded = function (event) {
    db = event.target.result;
    var table = IDBSetting.tables[0].temperature;
    var objectStore = db.createObjectStore(table.tableName, { keyPath: table.keyPath });
    // Setting indexes
    for (var i = 0; i < table.index.length; i++) {
      objectStore.createIndex(table.index[i], table.index[i], { unique: table.unique[i] });
    }

    var table = IDBSetting.tables[0].precipitation;
    var objectStore = db.createObjectStore(table.tableName, { keyPath: table.keyPath });
    // Setting indexes
    for (var i = 0; i < table.index.length; i++) {
      objectStore.createIndex(table.index[i], table.index[i], { unique: table.unique[i] });
    }
  };
}

///// Gets data from indexedDB. Plot redraw sequence start.
// Loop over years
function readNext(j = 0) {
  var yrsLen = yrs[1] - yrs[0]; // Calculate years in chosen range
  var currentYear = yrs[0] + j; // Initial value set

  // Reset range values
  if (j === 0) {
    show[page] = [];
    // Show loader
    loader(true);
  }

  // Looped over years range?
  if (j - yrsLen > 0) {
    if (show[page].length === 0) {
      // IDB seems empty fetching JSON data from server
      loadDoc();
    } else {
      // Dots before RDP optimization
      document.getElementById("dotsBefore").innerText = show[page].length;
      // Worker reduces total number of dots
      var myWorker = new Worker('/js/worker.js');

      function handleMessageFromWorker(msg) {
        // save result
        show[page] = msg.data;
        // Dots after RDP optimization
        document.getElementById("dotsAfter").innerText = show[page].length;
        // Drawing the plot
        myDraw();
      }

      myWorker.addEventListener('message', handleMessageFromWorker);

      var eps = (yrs[1] - yrs[0] + 1) / 9; // Tolerance
      myWorker.postMessage([show[page], eps]);
    }
  }

  // Loop over years range
  if (j <= yrsLen) {
    var transaction = db.transaction(page, 'readonly');
    var objectStore = transaction.objectStore(page);
    var myIndex = objectStore.index('year'); // Set query index
    var getRequest = myIndex.getAll(currentYear); // Get all values by year

    getRequest.onsuccess = function () {
      // Loop over year months
      for (var i = 0; i < getRequest.result.length; i++) {
        show[page] = show[page].concat(getRequest.result[i].data);
      }

      j++;
      readNext(j); // Recursive functions call
    };
  }
}

///// Draws plot
function myDraw() {
  var canvas = page === 'temperature' ? canvasOne : canvasTwo;
  var ctxt = page === 'temperature' ? cotxt : cttxt;

  // Resize plot
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight - 60; // Window height - header height

  var height = page === 'temperature' ? canvas.height / 2 : canvas.height - padding;

  ctxt.beginPath();
  ctxt.strokeStyle = "#f1c40f";
  ctxt.lineWidth = 1;
  ctxt.moveTo(padding, height);

  var len = show[page].length - 1;
  // X coefficient
  var kx = (canvas.width - 2 * padding) / len;

  // Finding max Y value
  var max = 0;
  for (var i = 0; i <= len; ++i) {
    if (Math.abs(show[page][i]) > max) {
      max = Math.abs(show[page][i]);
    }
  }

  // Setting Y labels
  document.getElementById("startTemperatureAxis").innerText = '+' + max + modes[0][page].y;
  document.getElementById("finishTemperatureAxis").innerText = '-' + max + modes[0][page].y;

  // Y coefficient
  var ky = (height - padding) / max;

  // Finally drawing our plot
  // show[page] - dataset according our years range (yrs)
  for (var i = 0; i <= len; i++) {
    ctxt.lineTo(i * kx + padding, height - ky * show[page][i]);
  }

  ctxt.stroke();

  // Hide loader after drawing is finished
  loader(false);
}

///// Draws axises for temperature plot
// one = true / false -> temperature grid / precipitation grid
function axisesDraw() {
  axisDraw(true); // Draw grid for temperature plot
  axisDraw(false); // Draw grid for precipitation plot
}

function axisDraw(one = true) {
  var gtxt = one ? gotxt : gttxt;
  var grid = one ? gridOne : gridTwo;
  var canvas = one ? canvasOne : canvasTwo;

  // Resize grid
  grid.width = window.innerWidth;
  grid.height = window.innerHeight - 60; // Window height - header height

  wheight = window.innerHeight - 60; // Window height - header height

  // Retina sharp
  // grid.width = grid.width * 2
  // grid.height = grid.height * 2
  // wheight = wheight * 2

  var height = one ? wheight - padding : wheight - padding;
  var xaxis = one ? wheight / 2 : wheight - padding;

  // Some basic styling
  gtxt.strokeStyle = '#2980b9';
  gtxt.lineWidth = .5;
  gtxt.fillStyle = "#2980b9";

  // X Axis
  gtxt.beginPath();
  gtxt.moveTo(padding, xaxis);
  gtxt.lineTo(grid.width - padding, xaxis);

  // X Arrow
  gtxt.lineTo(grid.width - padding - 13, xaxis - 5);
  gtxt.moveTo(grid.width - padding, xaxis);
  gtxt.lineTo(grid.width - padding - 13, xaxis + 5);

  // Y Axis
  gtxt.moveTo(padding, grid.height - padding);
  gtxt.lineTo(padding, padding);

  // Y Arrow
  gtxt.lineTo(padding - 5, padding + 13);
  gtxt.moveTo(padding, padding);
  gtxt.lineTo(padding + 5, padding + 13);

  gtxt.stroke();

  // X/Y Dot
  gtxt.beginPath();
  gtxt.arc(padding, height, 3, 0, 2 * Math.PI);
  gtxt.fill();

  // Y Dashed lines
  var n = 5;
  var w = (grid.width - 2 * padding) / n;
  gtxt.strokeStyle = '#bdc3c7';
  gtxt.lineWidth = 1;
  gtxt.setLineDash([9, 6]);

  // Y Dashed Lines
  gtxt.beginPath();
  for (var i = 1; i <= n; i++) {
    x = padding + w * i;
    gtxt.moveTo(x, padding);
    gtxt.lineTo(x, grid.height - padding);
  }
  gtxt.stroke();

  // Y Dashed Lines Dots
  gtxt.beginPath();
  for (var i = 1; i < n; i++) {
    x = padding + w * i;
    gtxt.arc(x, xaxis, 2, 0, 2 * Math.PI);
  }
  gtxt.fill();

  // Unset Dashed lines
  gtxt.setLineDash([]);

  // X Dashed lines
  var n = 6;
  var h = (height - padding) / n;
  gtxt.strokeStyle = '#bdc3c7';
  gtxt.setLineDash([9, 6]);

  // X Dashed Lines
  gtxt.beginPath();
  for (var i = 0; i <= n; i++) {
    y = height - h * i;
    gtxt.moveTo(padding, y);
    gtxt.lineTo(grid.width - padding, y);
  }
  gtxt.stroke();

  // X Dashed Lines Dots
  gtxt.beginPath();
  for (var i = 1; i < n; i++) {
    y = height - h * i;
    gtxt.arc(padding, y, 2, 0, 2 * Math.PI);
  }
  gtxt.fill();

  // Unset Dashed lines
  gtxt.setLineDash([]);
}

///// Fetchs JSON file and save it's values to indexedDB
function loadDoc() {
  // Show loader
  loader(true);

  var xhttp = new XMLHttpRequest();
  var url = modes[0][page].url;
  // Opening XHR
  xhttp.open("GET", url, true);
  xhttp.send();

  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {

      // XHR is successful
      var temperature = JSON.parse(xhttp.responseText);
      var length = temperature.length;

      var yid = 0; // current id
      var s = []; // Array of T values of JSON
      var i = 0;
      var year = 0; // Current loop year

      var flds = temperature[0].t.split("-"); // Breaking down date
      var lastid = flds[0] + flds[1]; // ID = year + month. i.e. 201801
      var yid = lastid; // Initial value
      var next = true;

      putNext(); // Run query sequence

      function putNext() {
        var flds = []; // fields
        s = []; // Array of values

        while (next && i < length) {
          flds = temperature[i].t.split("-"); // Breaking down date
          tyid = flds[0] + flds[1]; // ID = year + month. i.e. 201801

          if (lastid === tyid) {
            year = flds[0]; // Current loop year
            yid = tyid;
            s.push(temperature[i].v); // Pushing current value to result array
            i++;
          } else {
            lastid = tyid;
            next = false;
            // console.log('else', tyid);
          }
        }

        next = true;
        // Make new year object
        var objct = [];
        objct.yid = parseInt(yid); //Year ID
        objct.year = parseInt(year);
        objct.data = s;

        // If loop is not finished continue saving values to IDB
        if (i <= length) {
          var transaction = db.transaction(page, "readwrite");
          var objectStore = transaction.objectStore(page);
          var add = objectStore.put(objct);

          add.onsuccess = function (e) {
            if (i < length) {
              putNext(); // Run recursive function
            } else {
              readNext();
            }
          };

          // Sir, we've encountered error...
          add.onerror = function (e) {
            console.log('err', e);
          };
        } else {
          // Saving loop is finished, it's time to redraw the plot
          readNext();
        }
      }
    }
  };
}

///// Input year handler
function handleValue(k, value) {
  var tmp = parseInt(value); // Raw input year value
  var value = yrsLimit[k]; // Default year value
  var oppositeValue = yrs[Math.abs(k - 1)]; // Default opposite year value: start -> finish / finish > start
  var prefix = ['start', 'finish'];

  if (!tmp.follows(oppositeValue, k)) {
    // Checking bounds: start cannot be later than finish
    yrs[k] = oppositeValue; // Set start year
    document.getElementById(prefix[k] + "Year").value = oppositeValue;
    readNext();
  } else if (tmp.between(yrsLimit[0], yrsLimit[1])) {
    // If in range of default years i.e.: 1881 - 2006
    yrs[k] = tmp; // Set start year
    document.getElementById(prefix[k] + "YearAxis").innerText = tmp;
    readNext();
  } else {
    // Else we only correct the value, that's it!
    document.getElementById(prefix[k] + "Year").value = value;
    yrs[k] = value; // Set start year
  }
}

///// Checking hash
function checkhash() {
  switch (location.hash) {
    case "#temperature":
      page = 'temperature';
      break;
    case "#precipitation":
      page = 'precipitation';
      break;
    default:
      page = 'temperature';
      break;
  }

  document.documentElement.className = page;

  reactive();
}

var loading;
///// Show loader while fetching JSON file from server
function loader(state) {
  if (state) {
    loading = setTimeout(function () {
      document.body.className = document.body.className.replace(" loading-mode", "");
      document.body.className += " loading-mode";
    }, 100);
  } else {
    window.clearTimeout(loading);
    document.body.className = document.body.className.replace(" loading-mode", "");
  }
}

///// Highlight active mode link
function reactive() {
  var btns = document.getElementsByClassName("nav-mode");
  // Loop through the buttons and add the active class to the current/clicked button
  for (var i = 0; i < btns.length; i++) {

    if (btns[i].href.split('#')[1] === page) {
      btns[i].className += " active";
    } else {
      btns[i].className = btns[i].className.replace(" active", "");
    }
  }
}

/////////////////////
///// LISTENERS /////
/////////////////////

///// Years Range: start year listener
document.getElementById("startYear").addEventListener("input", function (e) {
  // Only digits that === 4
  if (this.value.length === 4) {
    handleValue(0, this.value);
  }
});

///// Years Range: finish year listener
document.getElementById("finishYear").addEventListener("input", function (e) {
  // Only digits that === 4
  if (this.value.length === 4) {
    handleValue(1, this.value);
  }
});

///// Hash chage listener - app modes switch
window.onhashchange = function () {
  init();
};

var resizeId;
window.onresize = function (event) {
  clearTimeout(resizeId);
  resizeId = setTimeout(function () {
    init();
    axisesDraw();
  }, 50);
};

//////////////////////
///// MAIN LOGIC /////
//////////////////////
init(); // Load dataset or fetch it from server
axisesDraw(); // Draw Grid

/***/ })
/******/ ]);